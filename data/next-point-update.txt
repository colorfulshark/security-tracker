CVE-2021-42343
	[bullseye] - dask.distributed 2021.01.0+ds.1-2.1+deb11u1
CVE-2021-3654
	[bullseye] - nova 2:22.2.2-1+deb11u1
CVE-2021-40083
	[bullseye] - knot-resolver 5.3.1-1+deb11u1
CVE-2021-41270
	[bullseye] - symfony 4.4.19+dfsg-2+deb11u1
CVE-2021-35604
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2021-46667
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2021-46662
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2021-46659
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2022-24048
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2022-24050
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2022-24051
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2022-24052
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2021-46661
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2021-46663
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2021-46664
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2021-46665
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2021-46668
	[bullseye] - mariadb-10.5 1:10.5.15-0+deb11u1
CVE-2021-44917
	[bullseye] - gnuplot 5.4.1+dfsg1-1+deb11u1
CVE-2021-45379
	[bullseye] - glewlwyd 2.5.2-2+deb11u2
CVE-2021-23177
	[bullseye] - libarchive 3.4.3-2+deb11u1
CVE-2021-31566
	[bullseye] - libarchive 3.4.3-2+deb11u1
CVE-2021-43808
	[bullseye] - php-laravel-framework 6.20.14+dfsg-2+deb11u1
CVE-2021-43617
	[bullseye] - php-laravel-framework 6.20.14+dfsg-2+deb11u1
CVE-2021-32718
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-32719
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-22116
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2018-1279
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-36980
	[bullseye] - openvswitch 2.15.0+ds1-2+deb11u1
CVE-2022-0155
	[bullseye] - node-follow-redirects 1.13.1-1+deb11u1
CVE-2022-0536
	[bullseye] - node-follow-redirects 1.13.1-1+deb11u1
CVE-2021-45115
	[bullseye] - python-django 2:2.2.26-1~deb11u1
CVE-2021-45116
	[bullseye] - python-django 2:2.2.26-1~deb11u1
CVE-2021-45452
	[bullseye] - python-django 2:2.2.26-1~deb11u1
CVE-2022-21670
	[bullseye] - node-markdown-it 10.0.0+dfsg-2+deb11u1
CVE-2022-20698
	[bullseye] - clamav 0.103.5+dfsg-0+deb11u1
CVE-2021-3997
	[bullseye] - systemd 247.3-7
CVE-2020-18442
	[bullseye] - zziplib 0.13.62-3.3+deb11u1
CVE-2022-0235
	[bullseye] - node-fetch 2.6.1-5+deb11u1
CVE-2021-40516
	[bullseye] - weechat 3.0-1+deb11u1
CVE-2021-23518
	[bullseye] - node-cached-path-relative 1.0.2-1+deb11u1
CVE-2021-44273
	[bullseye] - e2guardian 5.3.4-1+deb11u1
CVE-2021-46671
	[bullseye] - atftp 0.7.git20120829-3.3+deb11u2
CVE-2022-24130
	[bullseye] - xterm 366-1+deb11u1
CVE-2022-21814
	[bullseye] - nvidia-graphics-drivers-tesla-450 450.172.01-1~deb11u1
	[bullseye] - nvidia-graphics-drivers 470.103.01-1~deb11u1
CVE-2022-21813
	[bullseye] - nvidia-graphics-drivers-tesla-450 450.172.01-1~deb11u1
	[bullseye] - nvidia-graphics-drivers 470.103.01-1~deb11u1
CVE-2021-3803
	[bullseye] - node-nth-check 2.0.0-1+deb11u1
CVE-2021-33623
	[bullseye] - node-trim-newlines 3.0.0-1+deb11u1
CVE-2022-23806
	[bullseye] - golang-1.15 1.15.15-1~deb11u3
CVE-2022-23772
	[bullseye] - golang-1.15 1.15.15-1~deb11u3
CVE-2022-23773
	[bullseye] - golang-1.15 1.15.15-1~deb11u3
CVE-2022-24921
	[bullseye] - golang-1.15 1.15.15-1~deb11u4
CVE-2021-4104
	[bullseye] - apache-log4j1.2 1.2.17-10+deb11u1
CVE-2022-23302
	[bullseye] - apache-log4j1.2 1.2.17-10+deb11u1
CVE-2022-23305
	[bullseye] - apache-log4j1.2 1.2.17-10+deb11u1
CVE-2022-23307
	[bullseye] - apache-log4j1.2 1.2.17-10+deb11u1
CVE-2021-44832
	[bullseye] - apache-log4j2 2.17.1-1~deb11u1
CVE-2021-43396
	[bullseye] - glibc 2.31-13+deb11u3
CVE-2022-23218
	[bullseye] - glibc 2.31-13+deb11u3
CVE-2022-23219
	[bullseye] - glibc 2.31-13+deb11u3
CVE-2021-33574
	[bullseye] - glibc 2.31-13+deb11u3
CVE-2022-24953
	[bullseye] - php-crypt-gpg 1.6.4-2+deb11u1
CVE-2022-23647
	[bullseye] - node-prismjs 1.23.0+dfsg-1+deb11u2
CVE-2021-39191
	[bullseye] - libapache2-mod-auth-openidc 2.4.9.4-1+deb11u1
CVE-2021-40874
	[bullseye] - lemonldap-ng 2.0.11+ds-4+deb11u1
CVE-2022-0534
	[bullseye] - htmldoc 1.9.11-4+deb11u2
CVE-2022-22719
	[bullseye] - apache2 2.4.53-1~deb11u1
CVE-2022-22720
	[bullseye] - apache2 2.4.53-1~deb11u1
CVE-2022-22721
	[bullseye] - apache2 2.4.53-1~deb11u1
CVE-2022-23943
	[bullseye] - apache2 2.4.53-1~deb11u1
CVE-2021-37155
	[bullseye] - wolfssl 4.6.0+p1-0+deb11u1
CVE-2021-38597
	[bullseye] - wolfssl 4.6.0+p1-0+deb11u1
CVE-2021-44718
	[bullseye] - wolfssl 4.6.0+p1-0+deb11u1
CVE-2022-25638
	[bullseye] - wolfssl 4.6.0+p1-0+deb11u1
CVE-2022-25640
	[bullseye] - wolfssl 4.6.0+p1-0+deb11u1
CVE-2022-23308
	[bullseye] - libxml2 2.9.10+dfsg-6.7+deb11u1
CVE-2022-0995
	[bullseye] - linux 5.10.106-1
CVE-2022-1011
	[bullseye] - linux 5.10.106-1
CVE-2022-23036
	[bullseye] - linux 5.10.106-1
CVE-2022-23037
	[bullseye] - linux 5.10.106-1
CVE-2022-23038
	[bullseye] - linux 5.10.106-1
CVE-2022-23039
	[bullseye] - linux 5.10.106-1
CVE-2022-23040
	[bullseye] - linux 5.10.106-1
CVE-2022-23041
	[bullseye] - linux 5.10.106-1
CVE-2022-23042
	[bullseye] - linux 5.10.106-1
CVE-2022-23960
	[bullseye] - linux 5.10.106-1
CVE-2022-24958
	[bullseye] - linux 5.10.106-1
CVE-2021-0561
	[bullseye] - flac 1.3.3-2+deb11u1
CVE-2021-45005
	[bullseye] - mujs 1.1.0-1+deb11u1
CVE-2022-27240
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2021-46709
	[bullseye] - phpliteadmin 1.9.8.2-1+deb11u1
CVE-2021-33120
	[bullseye] - intel-microcode 3.20220207.1~deb11u1
CVE-2021-0145
	[bullseye] - intel-microcode 3.20220207.1~deb11u1
CVE-2021-0127
	[bullseye] - intel-microcode 3.20220207.1~deb11u1
CVE-2021-43861
	[bullseye] - node-mermaid 8.7.0+ds+~cs27.17.17-3+deb11u1
CVE-2021-44906
	[bullseye] - node-minimist 1.2.5+~cs5.3.1-2+deb11u1
CVE-2022-24773
	[bullseye] - node-node-forge 0.10.0~dfsg-3+deb11u1
CVE-2022-24772
	[bullseye] - node-node-forge 0.10.0~dfsg-3+deb11u1
CVE-2022-24771
	[bullseye] - node-node-forge 0.10.0~dfsg-3+deb11u1
CVE-2022-0686
	[bullseye] - node-url-parse 1.5.3-1+deb11u1
